import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp(functions.config().firebase);

exports.notificationForStaffs = functions.firestore
    .document(`notification/{userId}`)
    .onCreate(async (snap, context) => {
        const id: string = snap.data().id;
        const msgTitle: string = snap.data().title;
        const data = snap.data();
        console.log({ id }, { data });
        console.log(context.params.userId);

        const firestoreRef = admin.firestore();
        const userStore = await firestoreRef.collection('staffs').get();
        await new Promise((resolve) => {

            const tokens: any = [];
            userStore.forEach((doc) => {
                console.log(doc.id, '=>', doc.data());
                const token = doc.data().deviceToken1;
                console.log("deviceToken", token);
                if (token) {
                    tokens.push(token);
                }
            });
            resolve(tokens);
        }).then((tokens: any) => {
            console.log('Tokens', { tokens });
            const messages =
            {
                notification:
                {
                    title: 'New Notification added',
                    body: `${msgTitle}`
                }
            };
            if (tokens.length > 0) {
                return admin.messaging().sendToDevice(tokens, messages)
                    .then(() => {
                        console.log('New collection Created push notification triggered' + new Date());
                    }).catch((err) => {
                        console.log(err);
                    })
            }
            return;

        })
    });


exports.parentsmessage = functions.firestore
    .document(`parentsmessage/{userId}`)
    .onCreate(async (snap, context) => {
        const classId: string = snap.data().classId;
        const studentId: string = snap.data().studentId;
        const data = snap.data();
        console.log({ classId }, { data });
        console.log(context.params.userId);

        const firestoreRef = admin.firestore();
        const firstName = (await firestoreRef.collection('students').doc(studentId).get()).get('firstName');
        const lastName = (await firestoreRef.collection('students').doc(studentId).get()).get('lastName');
        const userStore = await firestoreRef.collection('staffs').where('staffClass', '==', classId).get();
        await new Promise((resolve) => {
            const tokens: any = [];
            userStore.forEach((doc) => {
                console.log(doc.id, '=>', doc.data());
                const token = doc.data().deviceToken1;
                console.log("deviceToken", token);
                if (token) {
                    tokens.push(token);
                }
            });
            resolve(tokens);
        }).then((tokens: any) => {
            console.log('Tokens', { tokens });
            const messages =
            {
                notification:
                {
                    title: 'New Notification added',
                    body: `${firstName}  ${lastName}'s parent has sent you a message`
                }
            };
            if (tokens.length > 0) {
                return admin.messaging().sendToDevice(tokens, messages)
                    .then(() => {
                        console.log('New collection Created push notification triggered' + new Date());
                    }).catch((err) => {
                        console.log(err);
                    })
            }
            return;

        })
    });