import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Observable, timer } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public selectedIndex = 0;
  showSplash = true;

  public appPages = [
    {
      title: 'Dashboard',
      url: '/dashboard',
    },
    {
      title: 'Dairy Entry',
      url: '/dairy',
    },
    {
      title: 'Event Gallery',
      url: '/event',
    },
    { 
      title: 'Social',
      url: '/social',
    },
  ];
  // public footPages=[
  //   {
  //     title: 'Privacy Policy',
  //     url: '/privacy-policy', 
  //   },
  //   {
  //     title: 'Sign Out',
  //     url: '/login',
  //   }
  // ]

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private afAuth: AngularFireAuth,
    private storage: Storage,
    public router: Router


  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.backgroundColorByHexString('#2A8184');
      this.statusBar.styleLightContent();
      this.splashScreen.hide();

      timer(3000).subscribe(() => this.showSplash = false);
      this.afAuth.onAuthStateChanged((user) => {
        if (!user) {
          this.storage.get('introShown').then((result) => {
            if (!result) {
              this.router.navigate(['home']);
              this.storage.set('introShown', true);
            } else {
              this.router.navigate(['login']);
            }
          });

        } else {
          this.router.navigate(['dashboard']);
        }
      });



    });
  }


  signOut() {
    this.afAuth.signOut().then(() => {
      console.log('signedOut');
      this.router.navigate(['login'])
    })
  }

  goToPrivacyPolicy() {
    this.router.navigate(['privacy-policy']);
  }

}
