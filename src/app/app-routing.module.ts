import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
  // {
  //   path: 'folder/:id',
  //   loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  // },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardPageModule)
  },
  {
    path: 'dairy',
    loadChildren: () => import('./pages/dairy/dairy.module').then(m => m.DairyPageModule)
  },
  {
    path: 'dairy-entry/:id',
    loadChildren: () => import('./pages/dairy-entry/dairy-entry.module').then(m => m.DairyEntryPageModule)
  },
  {
    path: 'dairy-list',
    loadChildren: () => import('./pages/dairy-list/dairy-list.module').then(m => m.DairyListPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./pages/notifications/notifications.module').then(m => m.NotificationsPageModule)
  },
  {
    path: 'event',
    loadChildren: () => import('./pages/event/event.module').then(m => m.EventPageModule)
  },
  {
    path: 'eventgallery/:id/:type',
    loadChildren: () => import('./pages/eventgallery/eventgallery.module').then(m => m.EventgalleryPageModule)
  },
  {
    path: 'gallery-images/:id/:index',
    loadChildren: () => import('./pages/gallery-images/gallery-images.module').then(m => m.GalleryImagesPageModule)
  },
  {
    path: 'academic-calendar',
    loadChildren: () => import('./pages/academic-calendar/academic-calendar.module').then(m => m.AcademicCalendarPageModule)
  },
  {
    path: 'leaverequests/:staffClass',
    loadChildren: () => import('./pages/leaverequests/leaverequests.module').then(m => m.LeaverequestsPageModule)
  },
  {
    path: 'parents-message/:staffClass',
    loadChildren: () => import('./pages/parents-message/parents-message.module').then(m => m.ParentsMessagePageModule)
  },
  {
    path: 'attendance',
    loadChildren: () => import('./pages/attendance/attendance.module').then( m => m.AttendancePageModule)
  },
  {
    path: 'markattendance/:id/:className',
    loadChildren: () => import('./pages/markattendance/markattendance.module').then( m => m.MarkattendancePageModule)
  },
  {
    path: 'aboutschool',
    loadChildren: () => import('./pages/aboutschool/aboutschool.module').then( m => m.AboutschoolPageModule)
  },
  {
    path: 'notification-detail/:id/:title',
    loadChildren: () => import('./pages/notification-detail/notification-detail.module').then( m => m.NotificationDetailPageModule)
  },
  {
    path: 'dairy-detail/:id/:title/:classId',
    loadChildren: () => import('./pages/dairy-detail/dairy-detail.module').then( m => m.DairyDetailPageModule)
  },
  {
    path: 'social',
    loadChildren: () => import('./pages/social/social.module').then( m => m.SocialPageModule)
  },
  {
    path: 'privacy-policy',
    loadChildren: () => import('./pages/privacy-policy/privacy-policy.module').then( m => m.PrivacyPolicyPageModule)
  },
  {
    path: 'parentsmessage-detail',
    loadChildren: () => import('./pages/parentsmessage-detail/parentsmessage-detail.module').then( m => m.ParentsmessageDetailPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
