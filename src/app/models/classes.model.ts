export interface Classes {
    className: string;
    createdOn: Date;
    sort ?: number;
}
