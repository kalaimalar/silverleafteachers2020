export interface ContactFaculty {
    createdOn: Date;
    studentId: string;
    classId: string;
    msgTitle: string;
    msgDesc: string; 
    status: string;
}