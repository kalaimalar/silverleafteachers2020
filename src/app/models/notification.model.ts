export interface Notification{
    adminStatus: string;
    createdOn: Date;
    message: string;
    title: string;
}