export interface Events {
    adminStatus: string;
    createdOn: Date;
    date: Date;
    message: string;
    title: string;
}