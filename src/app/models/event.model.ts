export interface Event {
    createdOn: Date;
    eventDate: Date;
    eventDesc: string;
    eventTitle: string;
    eventType: string;
}