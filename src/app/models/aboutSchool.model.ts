export interface AboutSchool{
    aboutContent1: string;
    aboutContent2: string;
    aboutImg: string;
    createdOn:Date;
}