export interface Dairy {
    adminStatus: string;
    createdOn: Date;
    classRefId: string;
    teacherRefId: string;
    title: string;
    message: string;
}