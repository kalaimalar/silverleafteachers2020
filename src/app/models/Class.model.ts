export interface Class {
    academicYears: string;
    createdOn: Date;
    staffClass: string;
    staffsDocRef: string;
}