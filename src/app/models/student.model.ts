export interface Student {
    firstName: string;
    middleName: string;
    LastName: string;
    Email: string;
    primaryMobile: number;
    secondaryMobile: number;
    Address1: string;
    Address2: string;
    City: string;
    State: string;
    Pincode: number;
    dateOfBirth: string;
    academicYear: string;
    fathersName: string;
    mothersName: string;
    image: string;
    studentClass: string;
    studentGender: string;
    createdOn: string;
}

