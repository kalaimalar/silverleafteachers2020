export interface LeaveRequest {
    classId: string;
    createdOn: Date;
    fromDate: Date;
    reasonForLeave: string;
    status: string;
    studentId: string;
    toDate: Date;
}
