export interface Attendance{
    classRefId: string,
    createdOn: Date,
    date: string,
    editedOn ?: Date,
    status: boolean,
    studentRefId: string,
    teacherRefId: string
}