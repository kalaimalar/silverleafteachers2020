export interface Staff {
    Address1: string;
    Address2: string;
    City: string;
    Designation: string;
    Email: string;
    Pincode: number;
    State: string;
    academicYears: string;
    createdOn: Date;
    dateOfBirth: Date;
    dateOfJoining: Date;
    fatherHusbandName: string;
    fatherHusbandOccupation: string;
    fatherHusbandPhone: number;
    fatherHusbandQualification: string;
    firstName: string;
    image: string;
    lastName: string;
    martialStatus: string;
    middleName: string;
    primaryMobile: number;
    secondaryMobile: number;
    staffClass: string;
    staffGender: string;
    staffStatus: string;
    staffEmail: string;
}

