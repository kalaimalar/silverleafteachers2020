import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EventgalleryPage } from './eventgallery.page';

describe('EventgalleryPage', () => {
  let component: EventgalleryPage;
  let fixture: ComponentFixture<EventgalleryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventgalleryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EventgalleryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
