import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-eventgallery',
  templateUrl: './eventgallery.page.html',
  styleUrls: ['./eventgallery.page.scss'],
})
export class EventgalleryPage implements OnInit {

  eventsGallery: any[] = [];
  galleryList: any[] = [];
  eventMessage: any;
  eventgalleryId: any;
  eventType: any;
  
  constructor(
    public dataService: DataService,
    private actRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {
    this.eventgalleryId = this.actRoute.snapshot.paramMap.get('id');
    this.eventType = this.actRoute.snapshot.paramMap.get('type');
    console.log(this.eventgalleryId);
    this.dataService.getEventsMessage(this.eventgalleryId).subscribe((data: any) => {
      console.log(data);
      this.eventMessage = data.eventDesc;
    })
    console.log(this.eventgalleryId);
    this.dataService.getEventsGallery(this.eventgalleryId).subscribe((data) => {
      console.log(data);
      data.forEach(element => {
        this.eventsGallery.push({
          path: element.payload.doc.data().downloadURL
        })

      })
    })
  }


  goToGalleryImages(index) {
    console.log(this.eventsGallery);
    this.router.navigate([`gallery-images/${this.eventgalleryId}/${index}`])
  }

}
