import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EventgalleryPageRoutingModule } from './eventgallery-routing.module';

import { EventgalleryPage } from './eventgallery.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EventgalleryPageRoutingModule
  ],
  declarations: [EventgalleryPage]
})
export class EventgalleryPageModule {}
