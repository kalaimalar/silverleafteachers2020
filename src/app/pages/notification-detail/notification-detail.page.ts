import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.page.html',
  styleUrls: ['./notification-detail.page.scss'],
})
export class NotificationDetailPage implements OnInit {

  notificationId: any;
  notification$: Observable<any>;
  notificationTitle: any;
  // notification: any;
  constructor(
    private actRoute: ActivatedRoute,
    public dataService: DataService
  ) { }

  ngOnInit() {
    this.notificationId = this.actRoute.snapshot.paramMap.get('id');
    this.notificationTitle = this.actRoute.snapshot.paramMap.get('title');
    if (this.notificationId) {
      this.getNotification()
    }
  }
  getNotification() {
    this.notification$ = this.dataService.getSingleNotification(this.notificationId);
    // console.log(this.notification);
  }

}
