import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentsMessagePage } from './parents-message.page';

const routes: Routes = [
  {
    path: '',
    component: ParentsMessagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentsMessagePageRoutingModule {}
