import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-parents-message',
  templateUrl: './parents-message.page.html',
  styleUrls: ['./parents-message.page.scss'],
})
export class ParentsMessagePage implements OnInit {

  staffClass: any;
  message$: Observable<any>;

  constructor(
    private actRoute: ActivatedRoute,
    public dataService: DataService,
    public router: Router
  ) { }

  ngOnInit() {
    this.staffClass = this.actRoute.snapshot.paramMap.get('staffClass');
    this.message$ = this.dataService.getParentsMessage(this.staffClass);

  }
  goToParentsMessageDetail(message) {
    console.log({ message });
    message.data.msgId = message.msgId
    this.router.navigate(['parentsmessage-detail'], { queryParams: message.data });
  }

}
