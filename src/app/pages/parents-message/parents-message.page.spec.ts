import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentsMessagePage } from './parents-message.page';

describe('ParentsMessagePage', () => {
  let component: ParentsMessagePage;
  let fixture: ComponentFixture<ParentsMessagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentsMessagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentsMessagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
