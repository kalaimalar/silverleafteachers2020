import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentsMessagePageRoutingModule } from './parents-message-routing.module';

import { ParentsMessagePage } from './parents-message.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentsMessagePageRoutingModule
  ],
  declarations: [ParentsMessagePage]
})
export class ParentsMessagePageModule {}
