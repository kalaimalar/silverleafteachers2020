import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.page.html',
  styleUrls: ['./attendance.page.scss'],
})
export class AttendancePage implements OnInit {

  class$: Observable<any>;
  toppings: any
  constructor(
    public dataService: DataService,
    public router: Router
  ) { }

  ngOnInit() {
    this.class$ = this.dataService.getAllClasses();
  }

  takeAttendance(data) {
    console.log(this.toppings);
    this.dataService.getClass(this.toppings).subscribe((classData) => {
      console.log(classData);
      this.router.navigate([`markattendance/${this.toppings}/${classData.className}`])

    })
  }

}
