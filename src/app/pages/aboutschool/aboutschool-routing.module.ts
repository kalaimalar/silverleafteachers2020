import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutschoolPage } from './aboutschool.page';

const routes: Routes = [
  {
    path: '',
    component: AboutschoolPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AboutschoolPageRoutingModule {}
