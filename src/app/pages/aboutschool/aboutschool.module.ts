import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AboutschoolPageRoutingModule } from './aboutschool-routing.module';

import { AboutschoolPage } from './aboutschool.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AboutschoolPageRoutingModule
  ],
  declarations: [AboutschoolPage]
})
export class AboutschoolPageModule {}
