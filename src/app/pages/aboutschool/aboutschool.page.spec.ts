import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AboutschoolPage } from './aboutschool.page';

describe('AboutschoolPage', () => {
  let component: AboutschoolPage;
  let fixture: ComponentFixture<AboutschoolPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutschoolPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AboutschoolPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
