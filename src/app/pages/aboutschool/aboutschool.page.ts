import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-aboutschool',
  templateUrl: './aboutschool.page.html',
  styleUrls: ['./aboutschool.page.scss'],
})
export class AboutschoolPage implements OnInit {

  AboutSchool: any = [];
  cloudFiles: any = [];
  storageRef: any;

  constructor(
    public dataService: DataService
  ) { }

  ngOnInit() {
    this.dataService.getAboutSchool().subscribe((data: any) => {
      console.log(data);
      this.AboutSchool = data.map((e: any) => {
        return {
          docId: e.payload.doc.id,
          aboutContent1: e.payload.doc.data().aboutContent1,
          aboutContent2: e.payload.doc.data().aboutContent2,
          createdOn: e.payload.doc.data().createdOn,
          aboutImg: e.payload.doc.data().aboutImg
        };
      });
      console.log(this.AboutSchool);
      this.getFile(this.AboutSchool[0].aboutImg);
    })
  }

  getFile(fileRef) {
    this.cloudFiles = [];
    console.log(this.cloudFiles);
    firebase.storage().ref(fileRef).getDownloadURL().then(dataUrl => {
      console.log(dataUrl);
      this.storageRef = dataUrl;
    });

  }



}
