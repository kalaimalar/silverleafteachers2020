import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  skipText = 'Skip';

  @ViewChild(IonSlides, { static: false }) slides: IonSlides;

  constructor(
    private router: Router

  ) { }

  ngOnInit() {

  }

  slideChanged() {
    this.slides.isEnd().then(data => {
      data ? this.skipText = 'Login' : this.skipText = 'Skip';
    });
  }

  skip() {
    this.router.navigate(['login']);
  }
}
