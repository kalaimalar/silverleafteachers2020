import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LeaverequestsPageRoutingModule } from './leaverequests-routing.module';

import { LeaverequestsPage } from './leaverequests.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LeaverequestsPageRoutingModule
  ],
  declarations: [LeaverequestsPage]
})
export class LeaverequestsPageModule {}
