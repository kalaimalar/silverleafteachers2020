import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LeaverequestsPage } from './leaverequests.page';

describe('LeaverequestsPage', () => {
  let component: LeaverequestsPage;
  let fixture: ComponentFixture<LeaverequestsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaverequestsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LeaverequestsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
