import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeaverequestsPage } from './leaverequests.page';

const routes: Routes = [
  {
    path: '',
    component: LeaverequestsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeaverequestsPageRoutingModule {}
