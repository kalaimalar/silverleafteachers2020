import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-leaverequests',
  templateUrl: './leaverequests.page.html',
  styleUrls: ['./leaverequests.page.scss'],
})
export class LeaverequestsPage implements OnInit {

  staffClass: any;
  leaveRequests$: Observable<any>;

  constructor(
    private actRoute: ActivatedRoute,
    public dataService: DataService
  ) { }

  ngOnInit() {
    this.staffClass = this.actRoute.snapshot.paramMap.get('staffClass');
    console.log(this.staffClass);
    if (this.staffClass) {
      this.leaveRequests$ = this.dataService.getLeaveRequests(this.staffClass);
      console.log(this.leaveRequests$);
    }
  }

}
