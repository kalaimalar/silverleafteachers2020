import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GalleryImagesPage } from './gallery-images.page';

describe('GalleryImagesPage', () => {
  let component: GalleryImagesPage;
  let fixture: ComponentFixture<GalleryImagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryImagesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GalleryImagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
