import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GalleryImagesPage } from './gallery-images.page';

const routes: Routes = [
  {
    path: '',
    component: GalleryImagesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GalleryImagesPageRoutingModule {}
