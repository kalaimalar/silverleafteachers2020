import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-gallery-images',
  templateUrl: './gallery-images.page.html',
  styleUrls: ['./gallery-images.page.scss'],
})
export class GalleryImagesPage implements OnInit {

  eventgalleryId: any;
  eventsGallery: any[] = [];
  imgIndex: number;
  slideOptions = null;
  @ViewChild('slides', { static: true }) slides: IonSlides;

  constructor(
    private actRoute: ActivatedRoute,
    public dataService: DataService
  ) { }

  ngOnInit() {
    let initialSlide = 0;
    this.eventgalleryId = this.actRoute.snapshot.paramMap.get('id');
    this.imgIndex = Number(this.actRoute.snapshot.paramMap.get('index'));
    initialSlide = this.imgIndex;
    this.dataService.getEventsGallery(this.eventgalleryId).subscribe((data: any) => {
      data.forEach((element, index) => {
        this.eventsGallery.push({
          path: element.payload.doc.data().downloadURL
        })
      });
    });
    this.slideOptions = {
      initialSlide
    }
    // this.slides.update();
  }

}
