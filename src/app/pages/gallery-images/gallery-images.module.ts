import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GalleryImagesPageRoutingModule } from './gallery-images-routing.module';

import { GalleryImagesPage } from './gallery-images.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GalleryImagesPageRoutingModule
  ],
  declarations: [GalleryImagesPage]
})
export class GalleryImagesPageModule {}
