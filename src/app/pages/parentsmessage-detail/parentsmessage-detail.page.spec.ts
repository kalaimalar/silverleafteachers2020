import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentsmessageDetailPage } from './parentsmessage-detail.page';

describe('ParentsmessageDetailPage', () => {
  let component: ParentsmessageDetailPage;
  let fixture: ComponentFixture<ParentsmessageDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentsmessageDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentsmessageDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
