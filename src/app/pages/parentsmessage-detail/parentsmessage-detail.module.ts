import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentsmessageDetailPageRoutingModule } from './parentsmessage-detail-routing.module';

import { ParentsmessageDetailPage } from './parentsmessage-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentsmessageDetailPageRoutingModule
  ],
  declarations: [ParentsmessageDetailPage]
})
export class ParentsmessageDetailPageModule {}
