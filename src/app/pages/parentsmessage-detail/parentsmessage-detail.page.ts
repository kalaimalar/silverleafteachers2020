import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-parentsmessage-detail',
  templateUrl: './parentsmessage-detail.page.html',
  styleUrls: ['./parentsmessage-detail.page.scss'],
})
export class ParentsmessageDetailPage implements OnInit {

  classDetail$: Observable<any>;
  studentDetail$: Observable<any>;
  title: any;
  message: any;
  replyMessage: any;
  replyMessageBlock: boolean = false;
  msgId: any;
  replyDocsize: any;
  classId: any;
  replyMessage$: Observable<any>;
  replyContent: any;
  replyArea: boolean;
  teacherMessage: boolean = false;
  constructor(
    public activateRoute: ActivatedRoute,
    public dataService: DataService,
    public afs: AngularFirestore,
    public toastCtrl: ToastController,
    public router: Router
  ) { }

  ngOnInit() {
    this.activateRoute.queryParams.subscribe((params) => {
      // console.log({ params });
      this.msgId = params.msgId;
      this.title = params.msgTitle
      this.message = params.msgDesc
      this.classId = params.classId
      console.log(this.message);
      this.afs.collection('parentsmessage').doc(this.msgId).collection('teachersReply').valueChanges().subscribe((responseData) => {
        console.log({ responseData });
        if (responseData.length == 1) {
          this.teacherMessage = true;
          this.replyContent = responseData[0].replymessage
        } else {
          this.replyMessageBlock = true;
        }
      });
      if (params.classId) {
        this.classDetail$ = this.afs.doc(`classes/${params.classId}`).valueChanges()
        console.log(this.classDetail$)
      }
      if (params.studentId) {
        console.log(params.studentId);
        this.studentDetail$ = this.afs.doc(`students/${params.studentId}`).valueChanges();
        console.log(this.studentDetail$);
      }
    })
  }

  replyToParents() {
    this.replyArea = !this.replyArea;
  }

  sendReply() {
    this.afs.collection('parentsmessage').doc(this.msgId).collection('teachersReply').get().subscribe((data) => {
      this.replyDocsize = data.size;
      console.log(this.replyDocsize);
      if (this.replyDocsize < 1) {
        console.log('if working');
        this.afs.collection('parentsmessage').doc(this.msgId).collection('teachersReply').add({
          replymessage: this.replyMessage,
          classId: this.classId
        }).then((success) => {
          this.presentToast('Reply Sent Successfully...')
          this.router.navigate(['dashboard']);
        })
        // console.log({ teachersReply });
      } else {
        console.log('else working');
        this.presentToast("Sorry you can't reply anymore...")
      }
    })
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
