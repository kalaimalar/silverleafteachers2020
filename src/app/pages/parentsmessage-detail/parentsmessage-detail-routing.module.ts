import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentsmessageDetailPage } from './parentsmessage-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ParentsmessageDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentsmessageDetailPageRoutingModule {}
