import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-dairy-entry',
  templateUrl: './dairy-entry.page.html',
  styleUrls: ['./dairy-entry.page.scss'],
})
export class DairyEntryPage implements OnInit {

  dairyTitle: any;
  dairyInfo: any;
  classRefId: any;
  constructor(
    private actRoute: ActivatedRoute,
    public dataService: DataService,
    public loadingCtrl: LoadingController,
    public router: Router,
    public toastCtrl: ToastController
  ) { }

  ngOnInit() {
    this.classRefId = this.actRoute.snapshot.paramMap.get('id');
    console.log(this.classRefId);
  } 

  async addNewDairyEntry() {
    this.dataService.addNewDairyEntry(this.classRefId, this.dairyTitle, this.dairyInfo);
    const loading = await this.loadingCtrl.create({
      message: 'Adding Dairy...',
      translucent: true,
      duration: 1000
    });
    await loading.present(); 
    const toast = await this.toastCtrl.create({
      message: 'Dairy Entry added successfully...',
      duration: 2000
    });
    toast.present(); 
    this.router.navigate(['dashboard']);
  }


}
