import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DairyEntryPageRoutingModule } from './dairy-entry-routing.module';

import { DairyEntryPage } from './dairy-entry.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DairyEntryPageRoutingModule
  ],
  declarations: [DairyEntryPage]
})
export class DairyEntryPageModule {}
