import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DairyEntryPage } from './dairy-entry.page';

describe('DairyEntryPage', () => {
  let component: DairyEntryPage;
  let fixture: ComponentFixture<DairyEntryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DairyEntryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DairyEntryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
