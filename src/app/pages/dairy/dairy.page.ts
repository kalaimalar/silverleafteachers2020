import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dairy',
  templateUrl: './dairy.page.html',
  styleUrls: ['./dairy.page.scss'],
})
export class DairyPage implements OnInit {

  class: any;
  classes: any[] = [];
  constructor(
    public dataService: DataService,
    public router: Router
  ) { }

  ngOnInit() {
    this.class = this.dataService.getClasses().subscribe((data: any) => {
      this.classes = data.map(a => {
        return {
          data: a.payload.doc.data(),
          classId: a.payload.doc.id
        }
      })
      console.log(this.classes);
    });
  }

  classSelected(event) {
    const classId = event.detail.value;
    if (classId) {
      this.router.navigate([`dairy-entry/${classId}`])
    }
  }

  goToDairyList(){
    this.router.navigate(['dairy-list']);
  }
}
