import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {
  notification$: Observable<any>;

  constructor(
    public dataService: DataService,
    public router:Router
  ) { }

  ngOnInit() {
    this.notification$ = this.dataService.getAllNotifications();
  }

  goToNotificationDetail(item) {
    console.log(item.notificationId);
    const id = item.notificationId;
    this.router.navigate([`notification-detail/${id}/${item.data.title}`]);
  }

}
