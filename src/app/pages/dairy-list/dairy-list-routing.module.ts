import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DairyListPage } from './dairy-list.page';

const routes: Routes = [
  {
    path: '',
    component: DairyListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DairyListPageRoutingModule {}
