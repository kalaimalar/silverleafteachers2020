import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DairyListPageRoutingModule } from './dairy-list-routing.module';

import { DairyListPage } from './dairy-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DairyListPageRoutingModule
  ],
  declarations: [DairyListPage]
})
export class DairyListPageModule {}
