import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';
import { Dairy } from 'src/app/models/dairy.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dairy-list',
  templateUrl: './dairy-list.page.html',
  styleUrls: ['./dairy-list.page.scss'],
})
export class DairyListPage implements OnInit {
  dairy$: Observable<any>;
  diaryEntries$: Observable<any>;
  dairyDocument: any;
  constructor(
    public dataService: DataService,
    public router:Router
  ) { }

  ngOnInit() {
    // this.dataService.getDairies().subscribe((data) => {
    //   new Promise(resolve => {
    //     console.log({ data });
    //     this.dairyDocument = data.map(e => {
    //       const date = e.payload.doc.data().createdOn
    //       return {
    //         data: e.payload.doc.data(),
    //         date: new Date(date.seconds)
    //       }
    //     })
    //     resolve(this.dairyDocument);
    //   }).then((dairyDocument: any) => {
    //     console.log(dairyDocument);
    //     this.dairyDocument.forEach((e, index) => {
    //       this.dataService.getClass(e.data.classRefId).subscribe((data: any) => {
    //         this.dairyDocument[index].data.className = data.className;
    //       })
    //     })
    //     console.log(this.dairyDocument);
    //   })
    // });
    this.diaryEntries$ = this.dataService.getDairies();


  }


  goToDairyDetail(item) {
    console.log(item);
    console.log(item.diaryId);
    this.router.navigate([`dairy-detail/${item.diaryId}/${item.data.title}/${item.data.classRefId}`]);
  }

}
