import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DairyListPage } from './dairy-list.page';

describe('DairyListPage', () => {
  let component: DairyListPage;
  let fixture: ComponentFixture<DairyListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DairyListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DairyListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
