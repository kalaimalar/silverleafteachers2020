import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Staff } from 'src/app/models/staffs.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Class } from 'src/app/models/Class.model';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { Classes } from 'src/app/models/classes.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  staff$: Observable<any>;
  staffDoc: AngularFirestoreDocument<Staff>;
  staffId: any;
  classDoc: AngularFirestoreDocument<Class>;
  staff = [] as any;
  subscription: any;
  staffInfo$: Observable<any>;
  classInfo: AngularFirestoreDocument<Classes>;


  constructor(
    private storage: Storage,
    private afs: AngularFirestore,
    public router: Router,
    private platform: Platform,
    public dataService: DataService
  ) { }

  ngOnInit() {
    this.storage.get('staffId').then((result) => {
      this.staffId = result;
    }).then(() => {



      this.staffDoc = this.afs.doc(`staffs/${this.staffId}`);

      this.staff$ = this.staffDoc.snapshotChanges().pipe(map(actions => {
        const data = actions.payload.data();
        const classInfo = this.getClassInfo(data.staffClass);
        // console.log(classInfo);
        return { data, classInfo };
      }));
    });
  }


  getClassInfo(id) {
    console.log(id);
    const classInfo = this.afs.doc(`classes/${id}`);
    return classInfo.valueChanges();
  }



  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator['app'].exitApp();
    });
  }

  ionViewDidLeave() {
    this.subscription.unsubscribe();
  }

  getStaffClass(id) {
    this.classDoc = this.afs.doc(`staffs/${id}`);
    return this.classDoc.valueChanges();
  }


  goToDairy() {
    this.dataService.presentLoading('Loading Dairy...').then(() => {
      this.router.navigate(['dairy']);
    });
  }

  goToNotifications() {
    this.dataService.presentLoading('Loading Notificiations...').then(() => {
      this.router.navigate(['notifications']);
    });
  }

  goToEvents() {
    this.dataService.presentLoading('Loading Events...').then(() => {
      this.router.navigate(['event']);
    })
  }

  goToAcademicCalendar() {
    this.dataService.presentLoading('Loading Calendar...').then(() => {
      this.router.navigate(['academic-calendar']);
    })
  }

  goToLeaveRequests() {
    this.dataService.getStaffInfo(this.staffId).subscribe((staff: any) => {
      const staffClass = staff.staffClass;
      console.log({ staffClass });
      if (staffClass) {
        this.dataService.presentLoading('Loading Leave Requests...').then(() => {
          this.router.navigate([`leaverequests/${staffClass}`]);
        })
      }
    });
  }


  goToParentsMessage() {
    this.dataService.getStaffInfo(this.staffId).subscribe((staff: any) => {
      const staffClass = staff.staffClass;
      console.log({ staffClass });
      if (staffClass) {
        this.dataService.presentLoading('Loading Messages...').then(() => {
          this.router.navigate([`parents-message/${staffClass}`]);
        });
      }
    });
  }

  goToAttendance() {
    this.dataService.presentLoading('Loading Attendance...').then(() => {
      this.router.navigate(['attendance']);
    });
  }

  goToAboutSchool() {
    this.dataService.presentLoading('Loading Module...').then(() => {
      this.router.navigate(['aboutschool']);
    });
  }

}

