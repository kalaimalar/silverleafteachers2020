import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.page.html',
  styleUrls: ['./event.page.scss'],
})
export class EventPage implements OnInit {

  events: any[] = [];
  eventType: any;
  imgPath: any;

  constructor(
    public dataService: DataService,
    public router: Router,
  ) { }
  
  ngOnInit() {
    this.dataService.getEvent().subscribe((data: any) => {
      data.forEach(element => {
        new Promise((resolve) => {
          const eventId = element.payload.doc.data().eventType;
          this.dataService.getEventType(eventId).subscribe((res: any) => {
            this.eventType = res.eventType;
              this.dataService.getGalleryCoverImg(element.payload.doc.id).subscribe((res: any) => {
                if (res.length != 0) {
                  this.imgPath = res[0].downloadURL;
                } else {
                  this.imgPath = false;
                }
                resolve(this.eventType);
              })
          });
        }).then(() => {
          this.dataService.getGalleryCoverImg(element.payload.doc.id).subscribe((res: any) => {
            if (res.length != 0) {
              this.imgPath = res[0].downloadURL;
            }
          })
        }).then(() => {
          const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
          const date = element.payload.doc.data().eventDate.seconds;
          const month = monthNames[new Date(date * 1000).getMonth()];
          const day = new Date(date * 1000).getDate();
          this.events.push({
            // date: element.payload.doc.data().eventDate,
            id: element.payload.doc.id,
            month: month,
            day: day,
            eventDesc: element.payload.doc.data().eventDesc,
            eventTitle: element.payload.doc.data().eventTitle,
            eventType: this.eventType,
            date: element.payload.doc.data().eventDate,
            image: this.imgPath
          })
        }).then(() => {
        })
      });
    })
  }

  eventClicked(id,type) {
    this.router.navigate([`eventgallery/${id}/${type}`]);
  }

}
