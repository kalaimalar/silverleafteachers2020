import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import {
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from 'date-fns';
import { Subject } from 'rxjs';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import { Data } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { ClassField } from '@angular/compiler';
import { resolve } from 'url';

@Component({
  selector: 'app-academic-calendar',
  templateUrl: './academic-calendar.page.html',
  styleUrls: ['./academic-calendar.page.scss'],
})
export class AcademicCalendarPage implements OnInit {

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();
  activeDayIsOpen = false;

  academicyearId: any;

  eventsList: any[] = [];

  constructor(
    public dataService: DataService,

  ) { }

  ngOnInit() {
    const month = this.viewDate.getMonth() || new Date();
    const year = this.viewDate.getFullYear();
    this.getEventsList(year, month);
  }

  getEventsList(year, month) {
    this.dataService.getAcademicEvents(year, month).subscribe((data: any) => {
      console.log('service works');
      const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      this.eventsList = data.map(e => {
        console.log(data);
        const date = e.payload.doc.data().date.seconds;
        const month = monthNames[new Date(date * 1000).getMonth()];
        const day = new Date(date * 1000).getDate();
        return {
          month: month,
          day: day,
          title: e.payload.doc.data().title,
          message: e.payload.doc.data().message
        }
      })
      console.log(this.eventsList);
    })
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
    this.eventsList = [];
    console.log(this.viewDate.getMonth());
    const month = this.viewDate.getMonth();
    const year = this.viewDate.getFullYear();
    console.log(month, year);
    this.getEventsList(year, month);
  }

}
