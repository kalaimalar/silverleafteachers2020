import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { ToastController, LoadingController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-markattendance',
  templateUrl: './markattendance.page.html',
  styleUrls: ['./markattendance.page.scss'],
})
export class MarkattendancePage implements OnInit {

  classId: any;
  className: any;
  students$: Observable<any>;
  attendanceTakenToday = false;
  checkedAttendance$: Observable<any>;
  attendance = [];
  staffId: any;
  studentList: number;
  attendanceTaken = false;

  constructor(
    private actRoute: ActivatedRoute,
    public dataService: DataService,
    public storage: Storage,
    private http: HttpClient,
    public toastCtrl: ToastController,
    private afs: AngularFirestore,
    public router: Router,
    public loadingCtrl: LoadingController

  ) { }

  ngOnInit() {
    this.storage.get('staffId').then((result) => {
      console.log({ result });
      this.staffId = result;
    })
    this.classId = this.actRoute.snapshot.paramMap.get('id');
    this.className = this.actRoute.snapshot.paramMap.get('className');
    console.log(this.className);
    console.log(this.classId);
    if (this.classId) {
      this.students$ = this.dataService.getStudentsOfClass(this.classId);
      this.students$.subscribe((data) => {
        this.studentList = data.length;
      })
    }

    this.checkedAttendance$ = this.dataService.checkAttendance(this.classId);
    this.checkedAttendance$.subscribe((data) => {
      console.log(data);
      if (data.length > 0) {
        this.attendanceTakenToday = true;
      }
    })



  }

  markAttendance(student, e) {
    console.log(e.detail.checked);
    const toggleValue = e.detail.checked;
    const day = new Date().getDay();
    const date = new Date().getDate()
    const month = new Date().getMonth();
    const year = new Date().getFullYear();
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    const addedDate = date + '/' + monthNames[month] + '/' + year;

    const attendanceEntry = {
      studentRefId: student.studentId,
      classRefId: student.data.studentClass,
      teacherRefId: this.staffId,
      date: addedDate,
      createdOn: new Date(),
      status: toggleValue,
      editedOn: null,
      dateMonth: month,
      dateYear: year
    }


    const index = this.attendance.findIndex((e) => e.studentRefId === student.studentId);

    if (index === -1) {
      this.attendance.push(attendanceEntry);
    } else {
      this.attendance[index] = attendanceEntry;
    }


    console.log(this.attendance.length);
    console.log(this.studentList);
    console.log(this.attendance)

    if (this.studentList == this.attendance.length) {
      this.attendanceTaken = true;
    }

  }


  sendAbsentMessage(student, value) {
    console.log({ student });
    console.log({ value });
    const day = new Date().getDay();
    const date = new Date().getDate()
    const month = new Date().getMonth();
    const year = new Date().getFullYear();
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    const addedDate = date + '/' + monthNames[month] + '/' + year;
    const attendanceEntry = {
      studentRefId: student.studentId,
      classRefId: student.data.studentClass,
      teacherRefId: this.staffId,
      date: addedDate,
      createdOn: new Date(),
      status: false,
      editedOn: null,
      dateMonth: month,
      dateYear: year
    }

    console.log({ attendanceEntry });



    const index = this.attendance.findIndex((e) => e.studentRefId === student.studentId);
    console.log({ index });

    if (index === -1) {
      this.attendance.push(attendanceEntry);
    } else {
      this.attendance[index] = attendanceEntry;
    }

    if (this.studentList == this.attendance.length) {
      this.attendanceTaken = true;
    }



    const numbers = student.data.primaryMobile + ',' + student.data.secondaryMobile;
    const name = student.data.firstName + ',' + student.data.lastName;

    const today = new Date();
    // this.storage.get('attendanceClass').then((thedata) => {
    //   const stClass = thedata;

    // console.log(stClass);

    const message = 'Dear Parent, Your ward ' + name + '  of class ' + this.className + ' is absent on ' + today.toDateString() + ' with leave request.';
    const messageWithoutNotification = 'Dear Parent, Your ward ' + name + '  of class ' + this.className + ' is absent on ' + today.toDateString() + ' without leave request.';




    // Dear Parent, Your ward 'student name' of class 'classname' is absent on ""
    // this.dataProvider.sendSms(numbers, message);





    const that = this;
    this.presentToast();
    setTimeout(function () {




      if (numbers.length > 0) {
        // tslint:disable-next-line: max-line-lengt       
        // let toast = this.toastCtrl.create({
        //   message: 'Absent SMS Sent',
        //   duration: 2000,
        //   position: 'bottom'
        // });

        // toast.present();


        if (value === 'notification') {
          // this.presentToast();
          that.http.get('https://api.msg91.com/api/sendhttp.php?mobiles=' + numbers + '&authkey=278970Aorb8sXi5cefa371&route=4&sender=SILVER&message=' + message + '&country=91').subscribe(data => {

            console.log(data);
          }, error => {
            console.log(error);
          });
        } else {
          that.http.get('https://api.msg91.com/api/sendhttp.php?mobiles=' + numbers + '&authkey=278970Aorb8sXi5cefa371&route=4&sender=SILVER&message=' + messageWithoutNotification + '&country=91').subscribe(data => {

            console.log(data);
          }, error => {
            console.log(error);
          });
        }

      }
    }, 2000);


    // });

  }

  addAttendance() {
    this.presentLoading().then(()=>{
      this.attendance.forEach(element => {
        this.afs.collection('studentsAttendance').add(element);
      });
      // let toast = this.toastCtrl.create({
      //   message: 'Attendance added successfully...',
      //   duration: 2000,
      //   position: 'bottom'
      // });
      this.presentToast().then(()=>{
        this.router.navigate(['dashboard']);
      });

    });
    // setTimeout(() => {

    // }, 1000)
  }


  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Adding Attendance...',
      duration: 1000
    });
    await loading.present();
  }

  async presentToast() {
    const toast = await this.toastCtrl.create({
      message: 'Absent SMS Sent',
      duration: 2000
    });
    toast.present();
  }
}
