import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MarkattendancePage } from './markattendance.page';

describe('MarkattendancePage', () => {
  let component: MarkattendancePage;
  let fixture: ComponentFixture<MarkattendancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkattendancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MarkattendancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
