import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-social',
  templateUrl: './social.page.html',
  styleUrls: ['./social.page.scss'],
})
export class SocialPage implements OnInit {

  constructor(
    private iab: InAppBrowser
  ) { }

  ngOnInit() {
  }

  onFbButtonClick() {
    const browser = this.iab.create('https://www.facebook.com/silverleafacademytirupur', '_self');
    browser.insertCSS({ code: "body{color: #3C9EA1;" });
  }

}
