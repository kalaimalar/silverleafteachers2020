import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-dairy-detail',
  templateUrl: './dairy-detail.page.html',
  styleUrls: ['./dairy-detail.page.scss'],
})
export class DairyDetailPage implements OnInit {

  dairyId: any;
  dairyTitle: any;
  dairy$: Observable<any>;
  classRefId$: any;
  dairy: any;
  constructor(
    private actRoute: ActivatedRoute,
    public dataService: DataService,
    public afs: AngularFirestore
  ) { }

  ngOnInit() {
    this.dairyId = this.actRoute.snapshot.paramMap.get('id');
    this.dairyTitle = this.actRoute.snapshot.paramMap.get('title');
    const classId = this.actRoute.snapshot.paramMap.get('classId');
    console.log(classId);
    // console.log(this.notificationId);
    console.log(this.dairyId);
    console.log(this.dairyTitle);
    if (this.dairyId) {
      this.dataService.getSingleDairy(this.dairyId).subscribe((data) => {
        console.log({ data });
        this.dairy = data;
        console.log(this.dairy);
      });
    }
    if (classId) {
      // this.classRefId = this.dataService.getClass(classId);
      this.classRefId$ = this.afs.doc(`classes/${classId}`).valueChanges();
      // console.log(this.classRefId);

    }
  }

}
