import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DairyDetailPage } from './dairy-detail.page';

describe('DairyDetailPage', () => {
  let component: DairyDetailPage;
  let fixture: ComponentFixture<DairyDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DairyDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DairyDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
