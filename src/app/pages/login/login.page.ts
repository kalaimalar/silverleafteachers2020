import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { Staff } from 'src/app/models/staffs.model';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  verificationId: any = '';
  code = '';
  showCodeInput = false;
  mobileNumber;
  staffDocId: any;
  staff$: Observable<any>;
  staffCol: AngularFirestoreCollection<Staff>;
  userNumber:any;

  constructor(
    private storage: Storage,
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    public router: Router,
    public firebaseX: FirebaseX,
    public dataService: DataService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    console.log('showcodeInput', this.showCodeInput);
    this.showCodeInput = false;
  }

  onOtpSend() {

    if (this.mobileNumber) {

      this.staffCol = this.afs.collection('staffs', ref => {
        return ref.where('primaryMobile', '==', '+' + 91 + this.mobileNumber);
      });

      this.staff$ = this.staffCol.snapshotChanges().pipe(map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const designation = data.Designation;
          const staffId = a.payload.doc.id;
          this.staffDocId = a.payload.doc.id;
          this.storage.set('staffId', staffId);
          return { data, designation };
        });
      }));

      this.staff$.subscribe(snapshot => {
        if (snapshot.length === 0) {
          this.checksecondnumber();
        } else {
          this.userNumber='primary'
          this.storage.set('contactType', 'primaryMobile');
          this.storage.set('contactNumber', this.mobileNumber)
          this.sendotp();
          this.showCodeInput = true;
        }
      });



    } else {
      alert('Add Mobile Number')
    }
  }



  sendotp() {
    this.afAuth
      .signInWithPhoneNumber('+' + 91 + this.mobileNumber, new firebase.auth.RecaptchaVerifier
        (
          're-container', {
          size: 'invisible'
        }))
      .then((creds) => {
        alert(creds);
        this.verificationId = creds.verificationId;
        console.log(this.showCodeInput);
      })
      .catch(err => alert(err));
  }

  checksecondnumber() {
    this.staffCol = this.afs.collection('staffs', ref => {
      return ref.where('secondaryMobile', '==', '+' + 91 + this.mobileNumber);
    });

    this.staff$ = this.staffCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const designation = data.Designation;
        const staffId = a.payload.doc.id;
        this.staffDocId = a.payload.doc.id;
        this.storage.set('staffId', staffId);
        return { data, designation, staffId };
      });
    }))

    this.staff$.subscribe(snapshot => {
      if (snapshot.length === 0) {
        alert('User number not match! Contact Your School!')
      } else {
        this.userNumber='secondary'
        this.storage.set('contactType', 'secondaryMobile');
        this.storage.set('contactNumber', this.mobileNumber)
        this.sendotp();
        this.showCodeInput = true;
      }
    });
  }


  verify() {
    const signInCredential = firebase.auth.PhoneAuthProvider.credential(this.verificationId, "" + this.code);
    try {
      firebase.auth().signInWithCredential(signInCredential).then((success) => {
        console.log('login data', success);
        this.afAuth.currentUser.then((data => {
          console.log({ data });
          console.log(data.uid);
          const userId = data.uid;
          // this.getToken().then((token) => {
          //   // user.deviceToken = token;
          //   console.log({ token });
            // this.dataService.addTokenToStaffsCollection(this.staffDocId, token, userId).then(() => {
          //     this.firebaseX.onTokenRefresh().subscribe(refToken => {
          //       console.log({ refToken });
          //       this.dataService.addTokenToStaffsCollection(this.staffDocId, refToken, userId)
          //     })
          //   });
          // })
          this.getToken().then((token) => {
            // user.deviceToken = token;
            console.log({ token });
            if (this.userNumber == 'primary') {
              console.log('Primary number is logged in');
              this.dataService.adddeviceToken1ToStaffsCollection(this.staffDocId, token).then(() => {
                this.firebaseX.onTokenRefresh().subscribe(refToken => {
                  console.log({ refToken });
                  this.dataService.adddeviceToken1ToStaffsCollection(this.staffDocId, refToken)
                })
              });
  
            } else {
              console.log('Primary number is logged in');
              this.dataService.adddeviceToken2ToStaffsCollection(this.staffDocId, token).then(() => {
                this.firebaseX.onTokenRefresh().subscribe(refToken => {
                  console.log({ refToken });
                  this.dataService.adddeviceToken2ToStaffsCollection(this.staffDocId, refToken)
                })
              });
            }
            // this.dataService.addTokenToStudentsCollection(this.studentDocId, token, userId).then(() => {
            //   this.fcm.onTokenRefresh().subscribe(refToken => {
            //     console.log({ refToken });
            //     this.dataService.addTokenToStudentsCollection(this.studentDocId, refToken, userId)
            //   })
            // });
          })
        }))
      });
    } catch (e) {
      console.log(e);
    }
  }


  getToken() {
    return this.firebaseX.getToken();
  }

  goToEnterOtp() {
    this.showCodeInput = false;
  }
}
