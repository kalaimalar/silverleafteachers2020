import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Classes } from '../models/classes.model';
import { map } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { Dairy } from '../models/dairy.model';
import { LoadingController } from '@ionic/angular';
import { Staff } from '../models/staffs.model';
import { Observable } from 'rxjs';
import { Events } from '../models/events.model';
import { LeaveRequest } from '../models/leaveRequests.model';
import { Student } from '../models/student.model';
import { ContactFaculty } from '../models/contactFaculty.model';
import { Attendance } from '../models/attendance.model';
import { AboutSchool } from '../models/aboutSchool.model';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  staffs: any;
  staff: any;
  classCol: AngularFirestoreCollection<Classes>;
  dairyCol: AngularFirestoreCollection<Dairy>;
  teacherRefId: any;
  classDoc: AngularFirestoreDocument<Classes>;
  staffDoc: AngularFirestoreDocument<Staff>;
  notificationCol: AngularFirestoreCollection<Notification>;
  eventCol: AngularFirestoreCollection<Event>;
  eventsCol: AngularFirestoreCollection<Events>;
  leaveRequestsCol: AngularFirestoreCollection<LeaveRequest>;
  studentDoc: AngularFirestoreDocument<Student>;
  msgCol: AngularFirestoreCollection<ContactFaculty>;
  studentClassCol: AngularFirestoreCollection<Student>;
  checkAttendanceCol: AngularFirestoreCollection<Attendance>;
  aboutCol: AngularFirestoreCollection<AboutSchool>;


  constructor(
    private afs: AngularFirestore,
    private storage: Storage,
    public loadingCtrl: LoadingController
  ) { }

  async presentLoading(msg) {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: msg,
      duration: 1000
    });
    await loading.present();
  }

  getClasses() {
    this.classCol = this.afs.collection('classes', ref => {
      return ref.orderBy('className', 'asc');
    });
    return this.classCol.snapshotChanges()
  }

  addNewDairyEntry(classRefId, title, message) {
    this.storage.get('staffId').then((data) => {
      console.log(data);
      this.teacherRefId = data;
      if (this.teacherRefId) {
        console.log(this.teacherRefId);
        this.afs.collection('studentDiary').add({
          adminStatus: "1",
          createdOn: new Date(),
          classRefId: classRefId,
          message: message,
          title: title,
          teacherRefId: this.teacherRefId
        })
      }
    });
  }

  getDairies() {
    this.dairyCol = this.afs.collection('studentDiary', ref => {
      return ref.orderBy('createdOn', 'desc').limit(35);
    });
    return this.dairyCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const diaryId = a.payload.doc.id;
        const classRoom = this.getClass(data.classRefId);
        const teacher = this.getStaffInfo(data.teacherRefId);
        return { data, diaryId, classRoom, teacher };
      });
    }));


  }

  getSingleDairy(id) {
    console.log(id);
    return this.afs.collection('studentDiary').doc(id).valueChanges();
    // this.dairyCol.snapshotChanges()
  }


  getClass(id) {
    console.log(id);
    this.classDoc = this.afs.doc(`classes/${id}`);
    return this.classDoc.valueChanges();
  }

  getStaffInfo(id) {
    this.staffDoc = this.afs.doc(`staffs/${id}`);
    return this.staffDoc.valueChanges();
  }

  getAllNotifications() {
    this.notificationCol = this.afs.collection('notification', ref => {
      return ref.orderBy('createdOn', 'desc').limit(20);
    });
    return this.notificationCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const notificationId = a.payload.doc.id;
        return { data, notificationId };
      });
    }));
  }

  getSingleNotification(id) {
    return this.afs.collection('notification').doc(id).valueChanges()
  }

  getEvent() {
    this.eventCol = this.afs.collection('eventgallery', ref => {
      return ref.orderBy('createdOn', 'desc').limit(20);
    });
    return this.eventCol.snapshotChanges();
  }

  getEventType(id) {
    return this.afs.collection('eventtypes').doc(id).valueChanges()
  }

  getEventsMessage(id) {
    return this.afs.collection('eventgallery').doc(id).valueChanges();
  }
  getEventsGallery(id) {
    return this.afs.collection('eventgallery').doc(id).collection('gallery').snapshotChanges();
  }

  getAcademicEvents(year, month) {
    this.eventsCol = this.afs.collection('events', ref => {
      return ref.where('dateYear', '==', year).where('dateMonth', '==', month).orderBy('date', 'asc');
    })
    return this.eventsCol.snapshotChanges();
  }

  getLeaveRequests(id) {
    this.leaveRequestsCol = this.afs.collection('leaverequests', ref => {
      return ref.where('classId', '==', id).orderBy('fromDate', 'desc').limit(30);
    });
    // return this.leaveRequestsCol.snapshotChanges();
    return this.leaveRequestsCol.snapshotChanges().pipe(map(actions => {
      console.log(actions);
      return actions.map(a => {
        const data = a.payload.doc.data();
        const studentInfo = this.getStudent(data.studentId);
        const classInfo = this.getClass(data.classId);

        return { data, studentInfo, classInfo }
      })
    }))
  }


  getStudent(id) {
    this.studentDoc = this.afs.doc(`students/${id}`);
    return this.studentDoc.valueChanges();
  }

  getParentsMessage(id) {
    console.log(id);
    this.msgCol = this.afs.collection('parentsmessage', ref => {
      return ref.where('classId', '==', id).limit(30).orderBy('createdOn', 'desc');
    });

    return this.msgCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const msgId = a.payload.doc.id;
        const studentData = this.getStudent(data.studentId);
        const classData = this.getClass(data.classId);
        return { data, msgId, studentData, classData };
      });
    }));
  }

  getAllClasses() {
    this.classCol = this.afs.collection('classes', ref => {
      return ref.orderBy('className', 'asc');
    });


    return this.classCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const classId = a.payload.doc.id;
        return { data, classId };
      });
    }));
  }

  getStudentsOfClass(classId) {
    this.studentClassCol = this.afs.collection('students', ref => {
      return ref.where('studentClass', '==', classId).orderBy('firstName', 'asc');
    });

    return this.studentClassCol.snapshotChanges().pipe(map(actions => {
      console.log({ actions });
      return actions.map(a => {
        const data = a.payload.doc.data();
        const studentId = a.payload.doc.id;
        return { data, studentId };
      })
    }))
  }



  checkAttendance(value) {
    const day = new Date().getDay();
    const date = new Date().getDate()
    const month = new Date().getMonth();
    const year = new Date().getFullYear();
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    const today = date + '/' + monthNames[month] + '/' + year;

    this.checkAttendanceCol = this.afs.collection('studentsAttendance', ref => {
      return ref.where('date', '==', today).where('classRefId', '==', value).limit(2);
    });
    return this.checkAttendanceCol.valueChanges();

  }


  getAboutSchool() {
    this.aboutCol = this.afs.collection('aboutSchool', ref => {
      return ref.orderBy('createdOn', 'desc').limit(1);
    })
    return this.aboutCol.snapshotChanges();
  }


  addTokenToStaffsCollection(studentDocId, token, userId) {
    let user: any = {
      deviceToken: token,
      userId: userId
    }
    return this.afs.collection('staffs').doc(studentDocId).set(user, { merge: true });
  }


  adddeviceToken1ToStaffsCollection(staffDocId, token) {
    let user: any = {
      deviceToken1: token,
    }
    return this.afs.collection('staffs').doc(staffDocId).set(user, { merge: true });
  }

  adddeviceToken2ToStaffsCollection(staffDocId, token) {
    let user: any = {
      deviceToken2: token,
    }
    return this.afs.collection('staffs').doc(staffDocId).set(user, { merge: true });
  }

  getGalleryCoverImg(id) {
    return this.afs.collection('eventgallery').doc(id).collection('coverImg').valueChanges();
  }
  
}
